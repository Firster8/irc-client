import socket
import threading


def channel(channel):
    if not channel.startswith("#"):
        return "#" + channel
    return channel


def quit():
    client.send_cmd("QUIT", "Good bye!")
    client.stopped = True
    print("Quitting ...")
    exit(0)


class IRCSimpleClient:
    stopped = False

    def __init__(self, username, channel, server="irc.freenode.net", port=6667):
        self.username = username
        self.server = server
        self.port = port
        self.channel = channel

    def connect(self):
        self.conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.conn.connect((self.server, self.port))

    def get_response(self):
        return self.conn.recv(512).decode("utf8")

    def send_cmd(self, cmd, message):
        command = "{} {}\r\n".format(cmd, message)
        self.conn.send(command.encode("utf8"))

    def send_message_to_channel(self, message):
        command = "PRIVMSG {}".format(self.channel)
        message = ":" + message
        self.send_cmd(command, message)

    def join_channel(self):
        cmd = "JOIN"
        channel = self.channel
        self.send_cmd(cmd, channel)

    def print_response(self):
        while not self.stopped:
            resp = self.get_response()
            if resp:
                try:
                    msg = resp.strip().split(":")
                    print("\n< {}> {}\n".format(msg[1].split("!")[0], msg[2].strip()))
                except IndexError:
                    pass


username = input("Username: ")
channel = channel(input("Channel: "))

cmd = ""
joined = False
client = IRCSimpleClient(username, channel)
client.connect()
t = threading.Thread(target=client.print_response)

while(not joined):
    resp = client.get_response()
    print(resp.strip())
    if "No Ident response" in resp:
        client.send_cmd("NICK", username)
        client.send_cmd(
            "USER", "%s * * :%s" % (username, username))

    # we're accepted, now let's join the channel!
    if "376" in resp:
        client.join_channel()

    # username already in use? try to use username with _
    if "433" in resp:
        username = "_" + username
        client.send_cmd("NICK", username)
        client.send_cmd(
            "USER", "{} * * :{}".format(username, username))

    # if PING send PONG with name of the server
    if "PING" in resp:
        client.send_cmd("PONG", ":" + resp.split(":")[1])

    # we've joined
    if "366" in resp:
        joined = True
        t.start()
try:
    while(cmd != "/quit"):
        cmd = input("< {}> ".format(username)).strip()
        if cmd == "/quit":
            quit()
        if cmd and len(cmd) > 0:
            client.send_message_to_channel(cmd)
except KeyboardInterrupt:
    quit()
